<script>
  const user = <?= $kirby->user() ? json_encode($kirby->user()->toArray()) : 'false' ?>;
  <?php
    $kirby = kirby();
    $site = site();
    $kirby->impersonate('kirby');
    $nav = [];
    foreach ($site->children()->filterBy('template', 'category')->listed() as $firstLevelPage) {
      $navInfos = getNavInfos($firstLevelPage);
      
      if ($firstLevelPage->hasListedChildren()) {
        $navInfos['children'] = [];
        foreach ($firstLevelPage->children()->listed() as $subItem) {
          $navInfos['children'][] = getNavInfos($subItem);
        }
      }
      $nav[] = $navInfos;
    }

    $jsonContent = null;
    $dataPath = './content/' . $page->diruri() . '/json/data.json';
    try {
      if (file_exists($dataPath)) {
        $jsonContent = file_get_contents($dataPath);
      } else {
        // create json file
      }
    } catch (\Throwable $th) {
      throw new Exception($th, 1);
    }
    

    
    $childrenJsonContent = [];
    foreach ($page->children() as $child) {
      if ((string)$child->title() !== 'json') {
        $childContent = json_decode(file_get_contents('./content/' . $child->diruri() . '/json/data.json'));
        $childContent->url = $child->url();
        $childrenJsonContent[] = $childContent;
      }
    } 
  ?>
  const kirby = {
    content: <?= $jsonContent ?>,
    page: <?= json_encode($page->toArray()) ?>,
    subpages: <?= $page->hasChildren() ? json_encode($childrenJsonContent) : 'false' ?>,
    template: '<?= Str::ucfirst($page->intendedTemplate()) ?>',
    nav: <?= json_encode($nav) ?>
  }

  console.log('kirby', kirby)
</script>