<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title> LOCAL - MADY & Co - Pour une alimentation durable</title>

  <?php snippet('meta') ?>

  <link rel="apple-touch-icon" sizes="180x180" href="<?= url('assets') ?>/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?= url('assets') ?>/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?= url('assets') ?>/favicon/favicon-16x16.png">
  <link rel="manifest" href="<?= url('assets') ?>/favicon/site.webmanifest">
  <link rel="mask-icon" href="<?= url('assets') ?>/favicon/safari-pinned-tab.svg" color="#349873">
  <link href='https://unpkg.com/maplibre-gl@latest/dist/maplibre-gl.css' rel='stylesheet' />
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="theme-color" content="#ffffff">

  <!-- Seems to work without this script. Remove before upload if not necessary. -->
  <!-- <script type="module" src="http://localhost:5173/@vite/client" defer></script> -->
  <?php snippet('variables') ?>
  <script type="module" src="http://localhost:5173/src/main.js" defer></script>
  <script src='https://unpkg.com/maplibre-gl@latest/dist/maplibre-gl.js'></script>

  <!--========== COOKIES MANAGEMENT ==========-->
  <script defer>
    document.addEventListener('DOMContentLoaded', () => {
      document.body.addEventListener('cookies:saved', event => {
        if (event.detail.includes('analytics')) {
          location.reload()
        }
      })
    })
  </script>
  <style>
    .cookie-modal__check {
      left: 16px !important;
    }
    .cookie-modal__checkbox,
    .cookie-modal__checkbox:focus {
      appearance: none !important;
      -webkit-appearance: none !important;
      -moz-appearance: none !important;
      display: block !important;
      width: 20px !important;
      height: 20px !important;
      border-radius: 0 !important;
      border: 2px solid #000 !important;
      margin-right: 6px !important;
      flex-shrink: 0 !important;
    }
    .cookie-modal__checkbox:checked:focus {
      background-color: #000 !important;
    }
    .cookie-modal__button.primary {
      background-color: #fff !important;
      color: #000 !important;
    }
  </style>
  <?php if (isFeatureAllowed('analytics')): ?>
    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=GT-PLFNWVR3"></script>
    <script>
      window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'GT-PLFNWVR3');
    </script>
  <?php endif ?>
</head>
<body>
  <div id="app">
  </div>