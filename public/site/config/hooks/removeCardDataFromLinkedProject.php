<?php
$newsData = getFieldValues($page);
foreach ($page->linkedProjects()->toPages() as $project) {
    $dataPath = realpath('./content/' . $project->diruri() . '/json/data.json');
    $jsonData = file_get_contents($dataPath);
    $data = json_decode($jsonData, true);

    $updatedNews = [];

    if (isset($data['news'])) {
      $updatedNews = array_filter($data['news'], function($item) use($newsData) {
        return $item['title'] !== $newsData['title'];
      });
    }

    $data['news'] = $updatedNews;
    
    updateOrCreateJsonData($project, $data);
}