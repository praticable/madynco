<?php

function updateContentForPageAndParents($page) {
  while ($page) {
    $updatedContent = getFieldValues($page);

    if ($page->template() == 'projects-group' || $page->template() == 'project' || $page->template() == 'card') {
      $updatedContent['news'] = getNewsData($page);
    }

    updateOrCreateJsonData($page, $updatedContent);

    $page = $page->parent();
  }
}

updateContentForPageAndParents($newPage);
