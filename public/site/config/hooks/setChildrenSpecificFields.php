<?php

$isLocated = $newPage->enableLocation() == 'true' ? 'true' : 'false';
$isDated = $newPage->enableDate() == 'true' ? 'true' : 'false';

foreach ($newPage->childrenAndDrafts() as $child) {
    $child->update([
      'Islocated' => $isLocated,
      'Isdated' => $isDated
    ]);
}
