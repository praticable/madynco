<?php
$newsData = getFieldValues($newPage);
$newsData['url'] = $newPage->url();
$newsData['panelUrl'] = $newPage->panel()->url();

function updateProjectNews($page, $newsData, $newPage = null, $mode = 'update') {
  foreach ($page->linkedProjects()->toPages() as $project) {
    $isRemovedFromLinkedProjects = false;
    if ($mode === 'remove') {
      $id = basename($project->permalink());
      $isRemovedFromLinkedProjects = !str_contains($newPage->linkedProjects()->value(), $id);
    }

    if ($mode === 'update' || $isRemovedFromLinkedProjects) {
      $dataPath = realpath('./content/' . $project->diruri() . '/json/data.json');
      $jsonData = file_get_contents($dataPath);
      $data = json_decode($jsonData, true);

      $updatedNews = [];

      if (isset($data['news'])) {
        $updatedNews = array_filter($data['news'], function($item) use($newsData) {
          return $item['title'] !== $newsData['title'];
        });
      }

      if ($mode === 'update') {
        $updatedNews[] = $newsData;
      }

      $data['news'] = $updatedNews;
      
      updateOrCreateJsonData($project, $data);
    }
  }
}

updateProjectNews($newPage, $newsData);
updateProjectNews($oldPage, $newsData, $newPage, 'remove');