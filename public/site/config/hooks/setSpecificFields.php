<?php

$parentRepository = $page->parent()->template() == 'repository' ? $page->parent() : $page->parent()->parent();

$isLocated = $parentRepository->enableLocation() == 'true' ? 'true' : 'false';
$isDated = $parentRepository->enableDate() == 'true' ? 'true' : 'false';

$page->update([
  'Islocated' => $isLocated,
  'Isdated' => $isDated
]);