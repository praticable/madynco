<?php

use Kirby\Toolkit\V;

return [
  'debug' => true,
  'api' => [
    'allowInsecure' => true
  ],
  'panel' => [
    'css' => 'assets/css/custom-panel.css'
  ],
  'hooks' => [
    'page.create:after' => function ($page) {
        $dataPath = './content/' . $page->diruri() . '/json/data.json';
        mkdir(dirname($dataPath), 0777, true);
        file_put_contents($dataPath, '{}');

        if ($page->template() == 'card') {
            require_once(__DIR__ . '/./hooks/setSpecificFields.php');
        }
    },
    'page.update:after' => function ($newPage, $oldPage) {
        require_once(__DIR__ . '/./hooks/saveJsonData.php');

        if ($newPage->template() == 'news' || $newPage->template() == 'card' || $newPage->template() == 'project' || $newPage->template() == 'project-group') {
            require_once(__DIR__ . '/./hooks/addCardDataToLinkedProject.php');
        }

        if ($newPage->template() == 'repository' || $newPage->template() == 'projects') {
            require_once(__DIR__ . '/./hooks/setChildrenSpecificFields.php');
        }
    },
    'page.changeStatus:after' => function (Kirby\Cms\Page $newPage, Kirby\Cms\Page $oldPage) {
        // require_once(__DIR__ . '/./hooks/saveJsonData.php');

        if ($newPage->template() == 'news' || $newPage->template() == 'card' || $newPage->template() == 'project' || $newPage->template() == 'project-group') {
            require_once(__DIR__ . '/./hooks/addCardDataToLinkedProject.php');
        }

        if ($newPage->template() == 'repository' || $newPage->template() == 'projects') {
            require_once(__DIR__ . '/./hooks/setChildrenSpecificFields.php');
        }
    },
    'page.delete:before' => function ($page) {
        require_once(__DIR__ . '/./hooks/removeCardDataFromLinkedProject.php');
    },
    'file.update:after' => function ($newFile) {
        $newPage = $newFile->parent();
        $oldPage = $newPage;
        require_once(__DIR__ . '/./hooks/saveJsonData.php');

        if ($newPage->template() == 'card' || $newPage->template() == 'project' || $newPage->template() == 'project-group') {
            require_once(__DIR__ . '/./hooks/addCardDataToLinkedProject.php');
        }

        if ($newPage->template() == 'repository' || $newPage->template() == 'projects') {
            require_once(__DIR__ . '/./hooks/setChildrenSpecificFields.php');
        }
    }
  ],
  'routes' => [
    require_once(__DIR__ . '/./routes/submit-card.php'),
    require_once(__DIR__ . '/./routes/set-page-to-draft.php'),
    require_once(__DIR__ . '/./routes/import-WP-DB.php'),
    [
      'pattern' => '(:all)logout.php',
      'action'  => function () {
          $kirby = kirby();
          $user = $kirby->user();
          $user->logout();
          go('/');
      }
    ]
    ],
];
