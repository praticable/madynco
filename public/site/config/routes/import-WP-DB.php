<?php

function getCoordinates($string)
{
    preg_match('/"latitude";d:([-\d\.]+);/', $string, $matchesLatitude);
    preg_match('/"longitude";d:([-\d\.]+);/', $string, $matchesLongitude);

    $lat = $matchesLatitude[1] ?? null;
    $lon = $matchesLongitude[1] ?? null;

    return [
      'lat' => $lat,
      'lon' => $lon
    ];
}

function getReverseLocationData($lat, $lon)
{
    $url = 'https://nominatim.openstreetmap.org/reverse?lat=' . $lat . '&lon=' . $lon . '&format=jsonv2&limit=1';


    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $url,
      CURLOPT_USERAGENT => 'Mady&Co/1.0',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
    ));

    $response = json_decode(curl_exec($curl));

    curl_close($curl);

    return $response;
}

function extractQualityTags($string)
{
    // Catch tags in array
    preg_match_all('/"([^"]*)"/', $string, $tags);

    // Transform dot into space
    $tags = array_map(function ($tag) {
        return str_replace('.', ' ', $tag);
    }, $tags[1]);

    return $tags;
}

function categorizeItems($rawCategories)
{
    $categorizedItems = [
      'activity' => [],
      'distribution' => [],
    ];

    foreach ($rawCategories as $item) {
        switch ($item) {
            case 'producteur':
            case 'jardin':
                $categorizedItems['activity'][] = $item;
                break;
            case 'pecherie':
                $categorizedItems['activity'][] = 'pêcherie';
                break;
            case 'aquaculture':
                $categorizedItems['activity'][] = 'aquaculture';
                break;
            case 'panier':
            case 'amap':
                if (!in_array('panier', $categorizedItems['distribution'])) {
                    $categorizedItems['distribution'][] = 'panier';
                }
                break;
            case 'magasin':
            case 'superette':
            case 'epicerie':
                if (!in_array('boutique', $categorizedItems['distribution'])) {
                    $categorizedItems['distribution'][] = 'boutique';
                }
                break;
        }
    }

    return $categorizedItems;
}

function createButtons($rowData)
{
    $buttons = [];

    if ($rowData['facebook'] !== 'NULL') {
        $buttons[] = [
          'content' => [
            'text' => 'facebook',
            'link' => $rowData['facebook'],
            'type' => 'primary'
          ],
          "isHidden" => false,
          "type" => "mady-button",
          "id" => Kirby\Uuid\BlockUuid::generate()
        ];
    }

    if ($rowData['instagram'] !== 'NULL') {
        $buttons[] = [
          'content' => [
            'text' => 'instagram',
            'link' => $rowData['instagram'],
            'type' => 'primary'
          ],
          "isHidden" => false,
          "type" => "mady-button",
          "id" => Kirby\Uuid\BlockUuid::generate()
        ];
    }

    if ($rowData['twitter'] !== 'NULL') {
        $buttons[] = [
          'content' => [
            'text' => 'twitter',
            'link' => $rowData['twitter'],
            'type' => 'primary'
          ],
          "isHidden" => false,
          "type" => "mady-button",
          "id" => Kirby\Uuid\BlockUuid::generate()
        ];
    }

    if ($rowData['youtube'] !== 'NULL') {
        $buttons[] = [
          'content' => [
            'text' => 'youtube',
            'link' => $rowData['youtube'],
            'type' => 'primary'
          ],
          "isHidden" => false,
          "type" => "mady-button",
          "id" => Kirby\Uuid\BlockUuid::generate()
        ];
    }

    if ($rowData['linkedin'] !== 'NULL') {
        $buttons[] = [
          'content' => [
            'text' => 'linkedin',
            'link' => $rowData['linkedin'],
            'type' => 'primary'
          ],
          "isHidden" => false,
          "type" => "mady-button",
          "id" => Kirby\Uuid\BlockUuid::generate()
        ];
    }

    if ($rowData['website'] !== 'NULL') {
        $buttons[] = [
          'content' => [
            'text' => 'website',
            'link' => $rowData['website'],
            'type' => 'primary'
          ],
          "isHidden" => false,
          "type" => "mady-button",
          "id" => Kirby\Uuid\BlockUuid::generate()
        ];
    }

    return $buttons;
}

return [
  'pattern' => '/import-WP-DB.json',
  'method' => 'GET',
  'action' => function () {
      $filepath = '/assets/csv/test.csv';
      $data = [];
      $receiverPage = site()->find('ressources/producteurs-acteurs');

      try {
          if (($file = fopen(realpath(__DIR__ . '/../../../assets/csv/test.csv'), "r")) !== false) {
              $headers = fgetcsv($file);

              $newCards = [];

              while (($row = fgetcsv($file)) !== false) {
                  $rowData = array_combine($headers, $row);

                  $slug = Str::slug($rowData['name']);
                  $title = $rowData['name'];
                  $authorName = $rowData['name'];
                  $authorMail = $rowData['email'];
                  $description = Str::unhtml($rowData['text']) . '\n\n' . $rowData['where_to_buy'] . '\n\n' . $rowData['opening_hours'];

                  $qualityTags = extractQualityTags($rowData['tags']);

                  $rawCategories = explode('.', $rowData['category']);
                  $categories = categorizeItems($rawCategories);

                  $coordinates = getCoordinates($rowData['geolocation']);
                  $locationData = $coordinates['lat'] && $coordinates['lon'] ?
                    getReverseLocationData($coordinates['lat'], $coordinates['lon']) : false;


                  $buttons = createButtons($rowData);

                  $city = null;
                  if (isset($locationData->address->town)) {
                      $city = $locationData->address->town;
                  } elseif (isset($locationData->address->city)) {
                      $city = $locationData->address->city;
                  } elseif (isset($locationData->address->village)) {
                      $city = $locationData->address->village;
                  } elseif (isset($locationData->address->hamlet)) {
                      $city = $locationData->address->hamlet;
                  } else {
                      $city = null;
                  }

                  $draft = $receiverPage->createChild(
                      ['slug'     => $slug,
                      'template' => 'card',
                      'content' => [
                        'title'  => $title,
                        'authorName' => $authorName,
                        'authorMail' => $authorMail,
                        'description' => $description,
                        'buttons' => json_encode($buttons),
                        'isFromMady' => 'false',
                        'repositoryMetadata' => [
                          'categories' => [
                            [
                              'name' => 'Activité',
                              'tags' => [
                                  ['text' => 'Producteur', 'value' => 'Producteur'],
                                  ['text' => 'Commercant', 'value' => 'Commercant'],
                                  ['text' => 'Métiers de bouche', 'value' => 'Métiers de bouche'],
                                  ['text' => 'MADY', 'value' => 'MADY'],
                                  ['text' => 'Educateur au goût', 'value' => 'Educateur au goût'],
                                  ['text' => 'Association', 'value' => 'Association'],
                              ],
                              'value' => $categories['activity'],
                            ],
                            [
                              'name' => 'Mode de distribution',
                              'tags' => [
                                  ['text' => 'Panier', 'value' => 'Panier'],
                                  ['text' => 'Marché', 'value' => 'Marché'],
                                  ['text' => 'Boutique', 'value' => 'Boutique'],
                                  ['text' => 'Vente directe', 'value' => 'Vente directe'],
                                  ['text' => 'Drive', 'value' => 'Drive'],
                                  ['text' => 'Cueillette libre', 'value' => 'Cueillette libre'],
                                  ['text' => 'Distributeur automatique', 'value' => 'Distributeur automatique'],
                              ],
                              'value' => $categories['distribution'],
                            ],
                            [
                                'name' => 'Produits',
                                'tags' => [
                                    ['text' => 'Viande', 'value' => 'Viande'],
                                    ['text' => 'Oeufs', 'value' => 'Oeufs'],
                                    ['text' => 'Légumes', 'value' => 'Légumes'],
                                    ['text' => 'Fruits', 'value' => 'Fruits'],
                                    ['text' => 'Poisson', 'value' => 'Poisson'],
                                    ['text' => 'Fleures', 'value' => 'Fleures'],
                                    ['text' => 'Plante aromatique', 'value' => 'Plante aromatique'],
                                    ['text' => 'Tisanes', 'value' => 'Tisanes'],
                                    ['text' => 'Volaille', 'value' => 'Volaille'],
                                    ['text' => 'Porc', 'value' => 'Porc'],
                                    ['text' => 'Boeuf', 'value' => 'Boeuf'],
                                    ['text' => 'Produits laitiers chèvre', 'value' => 'Produits laitiers chèvre'],
                                    ['text' => 'Produits laitiers vache', 'value' => 'Produits laitiers vache'],
                                ],
                                'value' => [],
                            ],
                            [
                                'name' => 'Réseau',
                                'tags' => [
                                    ['text' => 'Mady&Co', 'value' => 'Mady&Co'],
                                ],
                                'value' => ['Mady&Co'],
                            ],
                            [
                                'name' => 'Signe de Qualité',
                                'tags' => [
                                    ['text' => 'Bio', 'value' => 'Bio'],
                                    ['text' => 'Nature&Progrès', 'value' => 'Nature&Progrès'],
                                ],
                                'value' => $qualityTags
                            ],
                            [
                                'name' => 'Commune',
                                'tags' => [
                                    ['text' => 'Brandérion', 'value' => 'Brandérion'],
                                    ['text' => 'Bubry', 'value' => 'Bubry'],
                                    ['text' => 'Calan', 'value' => 'Calan'],
                                    ['text' => 'Caudan', 'value' => 'Caudan'],
                                    ['text' => 'Cléguer', 'value' => 'Cléguer'],
                                    ['text' => 'Gâvres', 'value' => 'Gâvres'],
                                    ['text' => 'Gestel', 'value' => 'Gestel'],
                                    ['text' => 'Groix', 'value' => 'Groix'],
                                    ['text' => 'Guidel', 'value' => 'Guidel'],
                                    ['text' => 'Hennebont', 'value' => 'Hennebont'],
                                    ['text' => 'Inguiniel', 'value' => 'Inguiniel'],
                                    ['text' => 'Inzinzac-Lochrist', 'value' => 'Inzinzac-Lochrist'],
                                    ['text' => 'Lanester', 'value' => 'Lanester'],
                                    ['text' => 'Languidic', 'value' => 'Languidic'],
                                    ['text' => 'Lanvaudan', 'value' => 'Lanvaudan'],
                                    ['text' => 'Larmor-Plage', 'value' => 'Larmor-Plage'],
                                    ['text' => 'Locmiquélic', 'value' => 'Locmiquélic'],
                                    ['text' => 'Lorient', 'value' => 'Lorient'],
                                    ['text' => 'Ploemeur', 'value' => 'Ploemeur'],
                                    ['text' => 'Plouay', 'value' => 'Plouay'],
                                    ['text' => 'Pont-Scorff', 'value' => 'Pont-Scorff'],
                                    ['text' => 'Port-Louis', 'value' => 'Port-Louis'],
                                    ['text' => 'Quéven', 'value' => 'Quéven'],
                                    ['text' => 'Quistinic', 'value' => 'Quistinic'],
                                    ['text' => 'Riantec', 'value' => 'Riantec'],
                                    ['text' => 'Merlevenez', 'value' => 'Merlevenez'],
                                    ['text' => 'Kervignac', 'value' => 'Kervignac'],
                                    ['text' => 'Sainte-Hélène', 'value' => 'Sainte-Hélène'],
                                    ['text' => 'Plouhinec', 'value' => 'Plouhinec'],
                                    ['text' => 'Nostang', 'value' => 'Nostang'],
                                    ['text' => 'Arzano', 'value' => 'Arzano'],
                                    ['text' => 'Bannalec', 'value' => 'Bannalec'],
                                    ['text' => 'Baye', 'value' => 'Baye'],
                                    ['text' => 'Clohars-Carnoët', 'value' => 'Clohars-Carnoët'],
                                    ['text' => "Guilligomarc'h", 'value' => "Guilligomarc'h"],
                                    ['text' => 'Le Trévoux', 'value' => 'Le Trévoux'],
                                    ['text' => 'Locunolé', 'value' => 'Locunolé'],
                                    ['text' => 'Mellac', 'value' => 'Mellac'],
                                    ['text' => 'Moëlan-sur-Mer', 'value' => 'Moëlan-sur-Mer'],
                                    ['text' => 'Querrien', 'value' => 'Querrien'],
                                    ['text' => 'Quimperlé', 'value' => 'Quimperlé'],
                                    ['text' => 'Rédéné', 'value' => 'Rédéné'],
                                    ['text' => 'Riec-sur-Bélon', 'value' => 'Riec-sur-Bélon'],
                                    ['text' => 'Saint-Thurien', 'value' => 'Saint-Thurien'],
                                    ['text' => 'Scaër', 'value' => 'Scaër'],
                                    ['text' => 'Tréméven', 'value' => 'Tréméven'],
                                ],
                                'value' => $city,
                            ]
                          ]
                        ],
                        'cardMetadata' => strlen($rowData['phone']) > 0 ?
                          [[
                            'label' => 'Téléphone',
                            'value' => $rowData['phone']
                          ]] : [],
                        'location' => $locationData ? [
                          'lat' => (float) $locationData->lat,
                          'lon' => (float) $locationData->lon,
                          'city' => $city,
                          'region' => $locationData->address->state,
                          'country' => $locationData->address->country,
                          'postcode' => $locationData->address->postcode,
                          'osm' => $locationData->place_id,
                        ] : false,
                      ]]
                  );
              }
              fclose($file);

              $card = $draft->changeStatus('listed');
              $newCards[] = $card->title()->value();
              sleep(1);
          }

          return [
            'status' => 'success',
            'new-cards' => $newCards
          ];
      } catch (Exception $e) {
          echo '<pre>';
          echo $e;
          echo '</pre>';
      }
  }
];
