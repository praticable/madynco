<?php
return [
  'pattern' => '/submit-card.json',
  'method' => 'POST',
  'action' => function () {
    $jsonRequest = file_get_contents("php://input");
    $request = json_decode($jsonRequest);

    $site = site();
    $kirby = kirby();
    $kirby->impersonate('kirby');
    
    $page = $site->index()->findBy('title', $request->parentRepository);

    $isCoverSet = strlen($request->cover->name) > 0;

    $blocks = convertObjectToArray($request->blocks);

    $preparedBlocks = [];

    $preparedCategories = isset($request->categories) ? array_map(function($category) {
      return [
        'name' => $category->name,
        'tags' => array_map(function($tag) {
          return [
            'text' => $tag,
            'value' => $tag
          ];
        }, $category->tags),
        'value' => $category->selection
      ];
    }, $request->categories) : false;
    
    $locationData = $request->location ? getLocationData($request->location) : false;

    $city = null;

    if (isset($locationData->address->town)) {
        $city = $locationData->address->town;
    } elseif (isset($locationData->address->city)) {
        $city = $locationData->address->city;
    } elseif (isset($locationData->address->village)) {
        $city = $locationData->address->village;
    } elseif (isset($locationData->address->hamlet)) {
        $city = $locationData->address->hamlet;
    } else {
        $city = null;
    }  

    try {
      $newCard = $page->createChild([
        'isDraft' => true,
        'template' => 'card',
        'content' => [
          'title' => $request->title,
          'description' => isset($request->description) ? $request->description : '',
          'authorName' => $request->authorName,
          'authorMail' => $request->authorMail,
          'bodybuilder' => '',
          'islocated' => $locationData ? 'true' : 'false', 
          'isdated' => $request->date ? 'true' : 'false', 
          'location' => $locationData ? [
            'lat' => (float) $locationData->lat,
            'lon' => (float) $locationData->lon,
            'city' => $city,
            'region' => $locationData->address->state,
            'country' => $locationData->address->country,
            'postcode' => $locationData->address->postcode,
            'osm' => $locationData->place_id,
          ] : false,
          'date' => $request->date ? $request->date->startDate : false,
          'enddate' => $request->date && strlen($request->date->endDate) > 0 ? 
            $request->date->endDate : false,
          'isexpiring' => $request->date ? $request->date->isExpiring : false,
          'repositorymetadata' => [
            'categories' => $preparedCategories,
            'fields' => $request->freeFields
          ],
        ]
      ]);

      if ($isCoverSet) {
        $coverName = $request->cover->name;
        $coverAlt = strlen($request->cover->alt) > 0 ? $request->cover->alt : false;
        $coverKirbyFile = createKirbyFileFromB64($newCard, $request->parentRepository, $coverName, $request->cover->b64, $coverAlt);
      }

      foreach ($blocks as $block) {
        if ($block['type'] === 'image') {
          $fileName = $block['content']['name'];
          $alt = strlen($request->cover->alt) > 0 ? $request->cover->alt : false;
          $path = './assets/files/' . $fileName;
          $kirbyFile = createKirbyFileFromB64($newCard, $request->parentRepository, $fileName, $block['content']['b64'], $alt);

          // CREATE BLOCK
          $preparedBlock = new Kirby\Cms\Block([
            'content' => [
              'location' => 'kirby',
              'image' => $kirbyFile->fileName(),
            ],
            'type' => 'image'
          ]);
          $block = $preparedBlock->toArray();
          unlink($path);
        }
        $preparedBlocks[] = $block;
      }

      $builder = Kirby\Cms\Layouts::factory([
        [
            'columns' => [
                [
                    'width' => '1/1',
                    'blocks' => $preparedBlocks
                ],
            ],
        ]
      ]);

      $newCard->update([
        'bodybuilder' => json_encode($builder->toArray()),
        'headerimage' => $isCoverSet ? [
          'location' => 'kirby',
          'image' => $coverKirbyFile->fileName(),
          'alt' => strlen($request->cover->alt) > 0 ? $request->cover->alt : false
        ] : false
      ]);

      sendMailNotification($request, $newCard->panel()->url());

      return $newCard->toArray();
    } catch (\Throwable $th) {
      throw $th;
    }
  }
];