<?php

return [
  'pattern' => '/set-page-to-draft.json',
  'method' => 'POST',
  'action' => function() {
    try {
      $jsonRequest = file_get_contents("php://input");
      $request = json_decode($jsonRequest);

      if (!$request || !isset($request->pageTitle)) {
        http_response_code(400);
        return ['error' => 'Requête JSON invalide'];
      }

      $site = site();
      $page = $site->index()->findBy('title', $request->pageTitle);

      if (!$page) {
        http_response_code(404);
        return ['error' => 'Page introuvable'];
      }

      $page->changeStatus('draft');
      return ['success' => true];
    } catch (Exception $e) {
      http_response_code(500);
      return ['error' => 'Erreur interne du serveur'];
    }
  }
];