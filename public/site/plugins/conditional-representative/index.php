<?php

Kirby::plugin('adrienpayet/contional-representative', [
	'blueprints' => [
    'blocks/conditional-representative' => __DIR__ . '/src/blueprints/blocks/conditional-representative.yml'
  ],
  'fields' => [
    'create-or-select-page' => [
      'props' => [
        'template' => function (string $template) {
          return $template;
        },
        'query' => function (string $query = null) {
          return $query;
        },
      ],
    ]
  ]
]);
