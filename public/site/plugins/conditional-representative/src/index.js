import ConditionalRepresentative from "./components/ConditionalRepresentative.vue";
import CreateOrSelectPage from "./components/fields/CreateOrSelectPage.vue";

window.panel.plugin("praticable/contional-representative", {
  blocks: {
    "conditional-representative": ConditionalRepresentative,
  },
  fields: {
    "create-or-select-page": CreateOrSelectPage,
  },
});
