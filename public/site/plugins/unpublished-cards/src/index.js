import UnpublishedCardsSection from "./components/UnpublishedCardsSection.vue";

window.panel.plugin("adrienpayet/unpublished-cards", {
  sections: {
    "unpublished-cards": UnpublishedCardsSection,
  },
});
