<?php

Kirby::plugin('adrienpayet/unpublished-cards', [
	'sections' => [
    'unpublished-cards' => [
      'computed' => [
        'unpublishedCards' => function() {
          $site = site();
          $cards = $site->index(true)->filterBy('template', 'card');

          $unpublishedCards = [];

          foreach ($cards as $card) {
            if ($card->status() == 'draft') {
              $unpublishedCards[] = $card->toArray();
            }
          }
          
          return $unpublishedCards;
        }
      ]
    ]
  ]
]);
