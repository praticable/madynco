import LinkedNews from "./components/LinkedNews.vue";

window.panel.plugin("adrienpayet/linked-news", {
  sections: {
    "linked-news": LinkedNews,
  },
});
