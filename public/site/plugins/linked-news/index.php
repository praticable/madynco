<?php

function removeNewsFromJsonPageData($page, $news) {
  $dataPath = './content/' . $page->diruri() . '/json/data.json';;

  // Lire le contenu du fichier JSON
  $jsonData = file_get_contents($dataPath);

  // Décoder le JSON en tableau PHP
  $data = json_decode($jsonData, true);

  // Parcourir le tableau pour trouver et supprimer la news
  foreach ($data['news'] as $key => $newsItem) {
      if (isset($newsItem['title']) && $newsItem['title'] === $news->title) {
          unset($data['news'][$key]);
          break;
      }
  }

  // Encoder le tableau en JSON
  $newJsonData = json_encode($data, JSON_PRETTY_PRINT);

  // Écrire le JSON modifié dans le fichier
  file_put_contents($dataPath, $newJsonData);
}

Kirby::plugin('adrienpayet/linked-news', [
	'sections' => [
    'linked-news' => [
      'computed' => [
        'news' => function() {
          $dataPath = './content/' . $this->model()->diruri() . '/json/data.json';
          if (file_exists($dataPath)) {
            $jsonData = file_get_contents($dataPath);
          } else {
            mkdir(dirname($dataPath), 0777, true);
            file_put_contents($dataPath, '{}');
            $jsonData = file_get_contents($dataPath);
          }
          $data = json_decode($jsonData);
          return $data->news ?? [];
        },
        'pageUid' => function() {
          return $this->model()->uid();
        }
      ]
    ]
  ],
  'routes' => [
    [
      'pattern' => '/remove-unreferenced-news.json',
      'method' => 'POST',
      'action' => function () {
        $jsonRequest = file_get_contents("php://input");
        $request = json_decode($jsonRequest);
        $site = site();

        $news = $request->news;
        $page = $site->index()->findBy('uid', $request->pageUid);
        $referencedNews = [];
        
        foreach ($news as $item) {
          $correspondingPage = $site->index()->findBy('title', $item->title);
          if (!$correspondingPage) {
            removeNewsFromJsonPageData($page, $item);
          } else {
            $referencedNews[] = $item;
          }
        }

        return json_encode($referencedNews);
      }
    ]
    ],
]);
