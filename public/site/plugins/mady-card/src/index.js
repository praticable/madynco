import MadyCard from "./components/MadyCard.vue";

window.panel.plugin("praticable/mady-card", {
  blocks: {
    card: MadyCard,
  },
});
