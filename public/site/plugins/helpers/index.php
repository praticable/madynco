<?php

/**
 * @param array|object $data
 * @return array
 */
function convertObjectToArray($data)
{
    $result = [];
    foreach ($data as $key => $value)
    {
        $result[$key] = (is_array($value) || is_object($value)) ? convertObjectToArray($value) : $value;
    }
    return $result;
}

function getNavInfos($page) {
  return [
    'title' => $page->title()->value(),
    'uri' => $page->uri(),
    'url' => $page->url()
  ];
}

function convertImagesToFilesInLayouts($incomingLayouts, $type = 'layout', $title = false) {
  $layout = [
      'type' => $type,
      'rows' => [],
      'title' => $title
  ];

  foreach ($incomingLayouts as $incomingLayout) {
      $rows = [];

      foreach ($incomingLayout->columns() as $incomingColumn) {
          $columns = processBlocks($incomingColumn->blocks());
          $rows[] = $columns;
      }

      $layout['rows'][] = $rows;
  }

  return $layout;
}

function processBlocks($blocks) {
  $processedBlocks = [];

  foreach ($blocks as $block) {
      if ($block->image()->isNotEmpty() || $block->images()->isNotEmpty()) {
          $processedBlock = processImageBlock($block);
      } elseif ($block->type() === 'grid') {
        $type = 'collapsableSection';
        $processedBlock = convertImagesToFilesInLayouts($block->grid()->toLayouts(), $type, $block->title()->value());
      } elseif ($block->type() === 'conditional-representative') {
        $processedBlock = getRepresentativeData($block, 'block');
      } else {
        $processedBlock = $block->toArray();
      }

      $processedBlocks[] = $processedBlock;
  }

  return $processedBlocks;
}

function getFileData($file, $variant = false) {
if (!$file) return false;

return [
  'srcset' => $file->croppedImage()->srcset([300, 800, 1024]),
  'url' => $file->croppedImage()->resize(1024)->url(),
  'alt' => $file->croppedImage()->alt()->value() ?? false,
  'name' => $file->croppedImage()->partnerName()->value() ?: $file->croppedImage()->itemName()->value() ?: false,
  'link' => $file->croppedImage()->link()->value() ?? false,
  'caption' => Str::unhtml($file->croppedImage()->caption()->value()) ?? false,
  'variant' => $variant
];
}

function processImageBlock($block) {
  $processedBlock = $block->toArray();

  if ($block->image()->isNotEmpty()) {
      $file = $block->image()->toFile();
      $variant = $block->type() === 'card' ? 'card' : false;
      $processedBlock['content']['image'] = getFileData($file, $variant);
  } elseif ($block->images()->isNotEmpty()) {
      $imagesData = [];
      
      foreach ($block->images()->toFiles() as $file) {
          $imagesData[] = getFileData($file);
      }

      $processedBlock['content']['images'] = $imagesData;
  }

  return $processedBlock;
}  

function getRepresentativeData($data, $mode) {
  if ($mode === 'block') {
    if ($data->representativeTemplate() == 'repository') {        
      $representedPage = $data->repositoryRepresentative()->toPage();
    } elseif ($data->representativeTemplate() == 'project') {      
      $representedPage = $data->projectRepresentative()->toPage();
    } elseif ($data->representativeTemplate() == 'card') {
      $representedPage = $data->cardRepresentative()->toPage();
    }
  } else {
    $representedPage = $data;
  }

  $representativeData = $representedPage->toArray();

  $representativeData['type'] = 'representative';
  $representativeData['variant'] = $mode === 'block' ? $data->representativeTemplate() : (string)$representedPage->template();
  $representativeData['url'] = $representedPage->url();
  $headerImage = $representedPage->headerImage();
  $representativeImage = $representedPage->representativeImage();

  if ($headerImage->exists() && $headerImage->isNotEmpty()) {
      $representativeData['content']['headerimage'] = getFileData($headerImage->toFile());
  } elseif ($representativeImage->exists() && $representativeImage->isNotEmpty()) {
      $representativeData['content']['headerimage'] = getFileData($representativeImage->toFile());
  } else {
      $representativeData['content']['headerimage'] = false;
  }
  $representativeData['content']['description'] = $representedPage->description()->value();
  
  $representativeData['content']['metadata'] = Yaml::decode($representedPage->repositoryMetadata()->value());  

  return $representativeData;
}

function getFieldValues($page) {
  $updatedContent = [];
  foreach ($page->content()->fields() as $key => $value) {
    if (str_contains($key, 'builder')) {
      $value = convertImagesToFilesInLayouts($value->toLayouts());
    } elseif (str_contains((string)$value, 'file://')) {
      
      $files = [];
      foreach ($value->toFiles() as $file) {
        $files[] = getFileData($file);
      }

      $value = $files;
    } elseif ($key === 'categories') {
      $categories = Yaml::decode($value->value());
      
      $value = array_map(function($category) {
        return [
          'name' => $category['name'],
          'tags' => array_map('trim', explode(',', $category['tags']))
        ];
      }, $categories);
    } elseif ($key === 'location') {
      $value = $value->isNotEmpty() ? $value->yaml() : false;
    } elseif ($key === "description") {
      $value = $value->kt()->value();
    } elseif ($key === "externalnews") {
      $externalNews = [];
      foreach ($value->toPages() as $item) {
        $externalNews[] = getFieldValues($item);
      }
      $value = $externalNews;
    } else {
      $value = $value->value();
    }
    
    $updatedContent[$key] = $value;
  }
  
  if ($page->hasChildren()) {
    foreach ($page->children() as $child) {
      if ($child->title() != 'json') {
        if ($page->template() == 'projects' || $page->template() == 'repository') {
          $updatedContent['children'][] = getRepresentativeData($child, 'page');
        } else {
          $updatedContent['children'][] = getFieldValues($child);
        }
      }
    }
  }

  $updatedContent['status'] = $page->status();

  return $updatedContent;
}

function createKirbyFileFromB64($page, $parentRepository, $fileName, $b64, $alt = false) {
  // CREATE TEMP FILE IN ASSETS FOLDER
  $path = './assets/files/' . $fileName;
  $file = fopen($path, "wb");
  fwrite($file, base64_decode($b64));
  fclose($file);

  if ($page->files()->has(strtolower($parentRepository) . '/' . $fileName)) {
      unlink($path);
      return json_encode('Files already exists');
      die();
  }

  // CREATE FILE IN PAGE FOLDER
  $kirbyFile = null;
  return $page->createFile([
    'source' => $path,
    'filename' => $fileName,
    'content' => [
      'alt' => $alt
    ]
  ]);
}

function updateOrCreateJsonData($page, $updatedContent) {
  $dataPath = './content/' . $page->diruri() . '/json/data.json';

  if (!file_exists($dataPath)) {
      if (!is_dir(dirname($dataPath))) {
          mkdir(dirname($dataPath), 0777, true);
      }
      
      file_put_contents($dataPath, '{}');
  }

  file_put_contents($dataPath, mb_convert_encoding(json_encode($updatedContent), 'UTF-8', 'HTML-ENTITIES'));
}

function removeAccents($string) {
  $accents = array(
    'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'à', 'á', 'â', 'ã', 'ä', 'å', 
    'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 
    'È', 'É', 'Ê', 'Ë', 'è', 'é', 'ê', 'ë', 
    'Ç', 'ç', 
    'Ì', 'Í', 'Î', 'Ï', 'ì', 'í', 'î', 'ï', 
    'Ù', 'Ú', 'Û', 'Ü', 'ù', 'ú', 'û', 'ü', 
    'ÿ', 
    'Ñ', 'ñ'
  );

  $withoutAccents = array(
    'A', 'A', 'A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a', 'a', 
    'O', 'O', 'O', 'O', 'O', 'O', 'o', 'o', 'o', 'o', 'o', 'o', 
    'E', 'E', 'E', 'E', 'e', 'e', 'e', 'e', 
    'C', 'c', 
    'I', 'I', 'I', 'I', 'i', 'i', 'i', 'i', 
    'U', 'U', 'U', 'U', 'u', 'u', 'u', 'u', 
    'y', 
    'N', 'n'
  );

  return str_replace($accents, $withoutAccents, $string);
}

function getLocationData($location) {
  $query = str_replace(' ', '+', removeAccents($location));
  $url = 'https://nominatim.openstreetmap.org/search?format=jsonv2&limit=1&addressdetails=1&q=' . $query;
  
  $curl = curl_init();
  
  curl_setopt_array($curl, array(
    CURLOPT_URL => $url,
    CURLOPT_USERAGENT => 'Mady&Co/1.0',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'GET',
  ));
  
  $response = json_decode(curl_exec($curl));

  curl_close($curl);
  
  if (count($response) > 0) {    
    return $response[0];
  } else {
    return false;
  }
}

function parseCategories($categories) {
  array_map(function($category) {
    return [
      'name' => $category->name,
      'tags' => array_map(function($tag) {
        return [
          'text' => $tag,
          'value' => $tag
        ];
      }, $category->tags),
      'value' => $category->selection
    ];
  }, $categories);
}

function sendMailNotification($request, $panelUrl) {
  $site = site();
  $from = 'madynco.fr <no-reply@madynco.fr>';
  $to = $site->emailNotification();
  $subject = 'madynco.fr - Nouvelle fiche ajoutée par un·e visiteur';
  
  $message = 'Bonjour,<br><br>'
          . $request->authorName . ' a ajouté une nouvelle fiche intitulée ' . $request->title . ' au répertoire ' . $request->parentRepository . '.<br>'
          . '<a href="'. $panelUrl .'">VOIR LA FICHE</a><br><br>'
          . 'Vous pouvez écrire à ' . $request->authorName . ' en répondant directement à ce mail (ou lui écrire plus tard via son mail disponible dans la fiche).';

  
  $headers = "MIME-version: 1.0\r\n".'Date: '.date('r')."\r\n";
  $headers .= "From: " . $from . " \r\n"."Reply-To: " . $request->authorName . " <" . $request->authorMail . "> \r\n"."Content-Type: text/html; charset=utf-8 \r\n";

  try {
    mail($to, $subject, $message, $headers);
  } catch (Exception $error) {
    $alert['error'] = "The form could not be sent";
    echo $alert;
  }
}

function getNewsData($page) {
  $dataPath = './content/' . $page->diruri() . '/json/data.json';

  if (file_exists($dataPath)) {
    $jsonData = file_get_contents($dataPath);
  } else {
    mkdir(dirname($dataPath), 0777, true);
    file_put_contents($dataPath, '{}');
    $jsonData = file_get_contents($dataPath);
  }

  $data = json_decode($jsonData);
  
  $news = $data->news ?? null;

  return $news;
}