import InheritedMetadataFields from "./components/fields/InheritedMetadataFields.vue";

panel.plugin("adrienpayet/inherited-metadata-fields", {
  fields: {
    "inherited-metadata-fields": InheritedMetadataFields,
  },
});
