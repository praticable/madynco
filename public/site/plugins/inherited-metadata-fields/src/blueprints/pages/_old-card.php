<?php

  $yaml = [
    'title'   => 'Fiche',
    'sections' => [
      'headerSection' => [
        'type' => 'fields',
        'fields' => [
          'parentFiltersCategory' => [
            "type" => "hidden",
            "value" => []
          ],
          'parentFiltersOptions' => [
            "type" => "hidden",
            "value" => []
          ],
          'cardTemplate' => [
            'type' => 'inherited-editable-structure',
            'provider' => 'filters',
            'locked' => 'true',
            "help" => "{{page.title}}",
            "page" => "{{page}}",
            'fields' => [
              'category' => [
                'label' => 'Catégorie de filtres',
                'type' => 'text',
                'width' => '1/2',
                "default" => "produits",
                "disabled" => "true"
              ],
              "options" => [
                "label" => "Filtres",
                "type" => "dynamic-input",
              ]
            ]
          ]
        ]
      ]
    ]
];

return $yaml;