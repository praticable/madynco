<?php

function getOptionsFromCards($cards) {
  $options = [];
  foreach ($cards->toPages() as $card) {
    
    $options[] = [
      'text' => $card->parent()->title() . ' / ' . $card->title(),
      'value' => $card->uri()
    ];
  }
  return $options;
}

Kirby::plugin('adrienpayet/inherited-metadata-fields', [
	'fields' => [
    'inherited-metadata-fields' => [
      'props' => [
        'label' => function(string $label) {
          return $label;
        },
        'providers' => function(array $providers) {
          return $providers;
        },
        'value' => function($value = null) {
          return Yaml::decode($value);
        },
        'inheritedCategories' => function() {          
          $provider = $this->providers()['categories'];
          $structure = $this->model()->parent()->{$provider}()->exists() ? 
            $this->model()->parent()->{$provider}()->toStructure() 
            : $this->model()->parent()->parent()->{$provider}()->toStructure();
          
          $categories = array_map(function($category) {
            return [
              'name' => $category['name'],
              'tags' => array_map(function($tag) {
                return [
                  'text' => $tag,
                  'value' => $tag
                ];
              }, array_map('trim', explode(',', $category['tags'])))
            ];
          }, $structure->toArray());
          
          return $categories;
        },
        'inheritedFields' => function() {
          $provider = $this->providers()['fields'];
          $fields = $this->model()->parent()->{$provider}()->exists() ? 
            $this->model()->parent()->{$provider}()->split() 
            : $this->model()->parent()->parent()->{$provider}()->split();
          
          return array_map(function($field) {
            return [
              'label' => $field,
              'value' => ''
            ];
          }, $fields);
        }
      ],
      'computed' => [
        'propertiesUrl' => function() {
          $parentRepository = $this->model()->parent()->template() == 'repository' ?
            $this->model()->parent()
            : $this->model()->parent()->parent();
          
          $propertiesUrl = $parentRepository->panel()->url() . '?tab=properties';
          
          return $propertiesUrl;
        },
      ]
    ]
  ]
]);
