<?php

Kirby::plugin('adrienpayet/refresh-data-button', [
  'sections' => [
    'refresh-data' => []
  ],
  'routes' => [
    [
      'pattern' => '/refresh-data.json',
      'method' => 'GET',
      'action' => function () {
          $site = site();

          foreach ($site->index() as $page) {
              if ($page->title()->value() !== 'json') {
                  $updatedContent = getFieldValues($page);

                  if ($page->template() == 'projects-group' || $page->template() == 'project' || $page->template() == 'card') {
                      $updatedContent['news'] = getNewsData($page);
                  }

                  updateOrCreateJsonData($page, $updatedContent);
              }
          }
          return json_encode('Refresh site data finished');
      }
    ]
  ]
]);
