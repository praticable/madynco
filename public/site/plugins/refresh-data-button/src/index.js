import RefreshDataButton from "./components/RefreshDataButton.vue";

window.panel.plugin("adrienpayet/refresh-data-button", {
  sections: {
    "refresh-data": RefreshDataButton,
  },
});
