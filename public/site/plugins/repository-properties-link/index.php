<?php

Kirby::plugin('adrienpayet/repository-properties-link', [
	'fields' => [
    'repository-properties-link' => [
      'computed' => [
        'url' => function() {
          $parentRepository = $this->model()->parent()->template() == 'repository' ?
            $this->model()->parent()
            : $this->model()->parent()->parent();
          
          $url = $parentRepository->panel()->url() . '?tab=properties';
          
          return $url;
        },
      ]
    ]
  ]
]);
