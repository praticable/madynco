import ParentPropertiesLink from "./components/ParentPropertiesLink.vue";

window.panel.plugin("adrienpayet/repository-properties-link", {
  fields: {
    "repository-properties-link": ParentPropertiesLink,
  },
});
