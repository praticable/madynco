import MadyButton from "./components/MadyButton.vue";

window.panel.plugin("praticable/mady-button", {
  blocks: {
    "mady-button": MadyButton,
  },
});
