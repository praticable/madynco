<?php

@include_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/lib/CookieMethods.php';

const DEFAULT_CONTENT = [
    'title' => 'Traces de visite',
    'text' => 'Afin d\'améliorer notre service, acceptez-vous que les traces de vos visites soient recueillies et étudiées via le service Google Analytics ?',
    'essentialText' => 'Essentielles',
    'denyAll' => 'Refuser tout',
    'acceptAll' => 'Accepter tout',
    'save' => 'Enregistrer',
];

Kirby::plugin('michnhokn/cookie-banner', [
    'snippets' => [
        'cookie-modal' => __DIR__ . '/snippets/cookie-modal.php',
        'cookie-modal-option' => __DIR__ . '/snippets/cookie-modal-option.php',
    ],
    'translations' => [
        'de' => [
            'michnhokn.cookie-banner' => DEFAULT_CONTENT
        ],
        'en' => [
            'michnhokn.cookie-banner.title' => 'Cookie settings',
            'michnhokn.cookie-banner.text' => 'We use cookies to provide you with the best possible experience. They also allow us to analyze user behavior in order to constantly improve the website for you. (link: privacy-policy text: Privacy Policy)',
            'michnhokn.cookie-banner.essentialText' => 'Essential',
            'michnhokn.cookie-banner.denyAll' => 'Reject All',
            'michnhokn.cookie-banner.acceptAll' => 'Accept All',
            'michnhokn.cookie-banner.save' => 'Save settings',
        ],
    ],
    'options' => [
        'features' => [
          'analytics' => 'Analytics',
        ],
        'content' => DEFAULT_CONTENT
    ]
]);
