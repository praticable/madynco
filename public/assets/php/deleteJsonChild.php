<?php

function deleteJsonChild($directory)
{
    if (is_dir($directory) && basename($directory) === "json") {
        $parentName = basename(dirname($directory));
        if ($parentName === "json") {
            emptyDirectory($directory);
            rmdir($directory);
            echo "Deleted directory: " . htmlspecialchars($directory) . "<br>";
            return;
        }
    }

    $items = scandir($directory);
    foreach ($items as $item) {
        if ($item === '.' || $item === '..') {
            continue;
        }
        $path = $directory . DIRECTORY_SEPARATOR . $item;
        if (is_dir($path)) {
            deleteJsonChild($path);
        }
    }
}

function emptyDirectory($directory)
{
    $items = scandir($directory);
    foreach ($items as $item) {
        if ($item === '.' || $item === '..') {
            continue;
        }
        $path = $directory . DIRECTORY_SEPARATOR . $item;
        if (is_dir($path)) {
            emptyDirectory($path);
            rmdir($path);
            echo "Deleted directory: " . htmlspecialchars($path) . "<br>";
        } else {
            unlink($path);
            echo "Deleted file: " . htmlspecialchars($path) . "<br>";
        }
    }
}

$directoryPath = __DIR__ . '/../../content';
echo "<h2>Starting the deletion process...</h2>";
deleteJsonChild(realpath(realpath($directoryPath)));
echo "<h2>Deletion process completed.</h2>";
