# Mady & co website

Doc to come.

# Vue 3 + Vite

This template should help get you started developing with Vue 3 in Vite. The template uses Vue 3 `<script setup>` SFCs, check out the [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup) to learn more.

## Recommended IDE Setup

- [VS Code](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).

# Development

## Start development environment

The code is based on [Kirby CMS](https://getkirby.com/) and [Vue 3](https://vuejs.org/). It needs a PHP and a Node server running in parallel.

```bash
# start the node server
npm run dev

# in another terminal, start the PHP server from the public directory
cd public/
php -S localhost:<port-number> kirby/router.php
```
