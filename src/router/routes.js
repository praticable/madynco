import Free from "../views/Free.vue"
import Agenda from "../views/Agenda.vue"
import Repository from "../views/Repository.vue"
import Projects from "../views/Projects.vue"

// For nav, see : /public/site/snippets/variables.php
// Parse needed to evaluate components
const parsedNav = nav.map((mainItems) => {
  if (mainItems.path === "/home") mainItems.path = "/"
  mainItems.component = eval(mainItems.component)
  if (mainItems.children) {
    mainItems.children = mainItems.children.map((child) => {
      child.component = eval(child.component)
      return child
    })
  }
  return mainItems
})

console.log("Routes : ", parsedNav)

export const routes = parsedNav
