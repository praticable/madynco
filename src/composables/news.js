import dayjs from "dayjs"
import "dayjs/locale/fr"

function isExpired(news) {
  const content = news.hasOwnProperty("content") ? news.content : news
  let isExpired = false

  const currentDate = dayjs()
  const newsDate = dayjs(content.date)
  const newsEndDate = dayjs(content.enddate)
  const isExpiring =
    content.isexpiring === true || content.isexpiring === "true"

  const isDateValid = (date) => date.isValid()
  const isExpiredDate = (date) => currentDate.diff(date, "day") > 1

  if (isExpiring && isDateValid(newsDate)) {
    if (
      isDateValid(newsEndDate)
        ? isExpiredDate(newsEndDate)
        : isExpiredDate(newsDate)
    ) {
      isExpired = true
      setPageToDraft(content.title)
    }
  }

  return isExpired
}

function setPageToDraft(pageTitle) {
  fetch("/set-page-to-draft.json", {
    method: "POST",
    body: JSON.stringify({
      pageTitle: pageTitle,
    }),
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error("Erreur lors de la requête fetch")
      }
      return response.json()
    })
    .then((data) => {
      console.log(`Page ${pageTitle} passée en brouillon.`)
    })
    .catch((error) => {
      console.error(
        `Erreur lors de la tentative de passage de la fiche ${pageTitle} en mode brouillon.`,
        error
      )
    })
}

export { isExpired, setPageToDraft }
