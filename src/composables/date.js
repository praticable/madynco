const monthsMap = {
  "01": "janvier",
  "02": "février",
  "03": "mars",
  "04": "avril",
  "05": "mai",
  "06": "juin",
  "07": "juillet",
  "08": "août",
  "09": "septembre",
  10: "octobre",
  11: "novembre",
  12: "décembre",
}

function removeLeadingZero(str) {
  if (str.startsWith("0")) {
    return str.substring(1)
  }
  return str
}

function formatDate(date, mode = "natural") {
  if (mode === "natural") {
    const parts = date.split("-")
    if (parts.length === 3) {
      const day = removeLeadingZero(parts[2])
      const month = monthsMap[parts[1]]
      const year = parts[0]
      return `${day} ${month} ${year}`
    } else {
      return ""
    }
  } else {
    const parts = date.split("-")
    date = parts.reverse()
    date[date.length - 1] = date[date.length - 1].slice(
      2,
      date[date.length - 1]
    )
    date = date.join("/")
    return date
  }
}

export { formatDate }
