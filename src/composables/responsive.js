import { ref } from "vue"
const device = ref(window.innerWidth > 800 ? "desktop" : "mobile")

window.addEventListener("resize", () => {
  if (window.innerWidth > 800) device.value = "desktop"
  if (window.innerWidth < 800) device.value = "mobile"
})

export { device }
