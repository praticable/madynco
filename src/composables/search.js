import { ref, computed } from "vue";
import dayjs from "dayjs";
import "dayjs/locale/fr";
import { isExpired } from "./news";

dayjs.locale("fr");

const items = ref({});
const search = ref("");
const categories = ref([]);
const metadata = ref([]);
const selectedDate = ref(dayjs());

const views = ref([
  {
    id: "grid",
    icon: "/assets/svg/grid.svg",
    isActive: true,
    disabled: false,
    label: "Vue mosaïque",
  },
  {
    id: "map",
    icon: "/assets/svg/map.svg",
    isActive: false,
    disabled: null,
    label: "Vue carte",
  },
  {
    id: "agenda",
    icon: "/assets/svg/agenda.svg",
    isActive: false,
    disabled: null,
    label: "Vue agenda",
  },
]);

const selectedTags = computed(() =>
  categories.value.map((category) => category.selection).flat()
);

const activeView = computed(() => {
  return views.value.find((view) => view.isActive);
});

const isTitleMatching = (item) =>
  item.content.title.toLowerCase().includes(search.value.toLowerCase());

const isTagsMatching = (item) => {
  const tags = item.content.metadata.hasOwnProperty("categories")
    ? item.content.metadata.categories
        ?.map((category) => category.value)
        .filter(Boolean)
        .flat()
    : [];

  const isTagsMatching =
    selectedTags.value.length === 0
      ? true
      : tags.some((tag) => selectedTags.value.includes(tag));
  return isTagsMatching;
};

const isDescriptionMatching = (item) =>
  item.content.description.includes(search.value);

const isDateMatching = (item) => {
  if (item.content.hasOwnProperty("date")) {
    const dateMatches =
      item.content.date === selectedDate.value.format("YYYY-MM-DD");
    const isAfterStartDate =
      selectedDate.value.isAfter(item.content.date) ||
      selectedDate.value.isSame(item.content.date);
    const isBeforeEndDate =
      item.content.enddate &&
      (selectedDate.value.isBefore(item.content.enddate) ||
        selectedDate.value.isSame(item.content.enddate));
    return dateMatches || (isAfterStartDate && isBeforeEndDate);
  }
  return true;
};

const filteredItems = computed(() => {
  const itemsArray = items.value ?? {}; // Utilise un objet vide si items.value est undefined
  const filteredItems = Object.values(itemsArray).filter((item) => {
    return (
      isTagsMatching(item) &&
      (activeView.value.id !== "agenda" || isDateMatching(item)) &&
      (isTitleMatching(item) || isDescriptionMatching(item)) &&
      !isExpired(item)
    );
  });
  return filteredItems;
});

export {
  items,
  search,
  categories,
  filteredItems,
  metadata,
  selectedDate,
  views,
};
